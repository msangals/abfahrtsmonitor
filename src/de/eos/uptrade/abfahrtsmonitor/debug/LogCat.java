package de.eos.uptrade.abfahrtsmonitor.debug;

import android.util.Log;
import de.eos.uptrade.abfahrtsmonitor.interfaces.DebugInterface;

public class LogCat {
	
	public static void v(String tag, String message) {
		if (DebugInterface.DEBUG) {
			Log.v(tag, message);
		}
	}
	
	public static void d(String tag, String message) {
		if(DebugInterface.DEBUG) {
			Log.d(tag, message);
		}
	}
	
	public static void i(String tag, String message) {
		if (DebugInterface.DEBUG) {
			Log.i(tag, message);
		}
	}
	
	public static void w(String tag, String message) {
		if (DebugInterface.DEBUG) {
			Log.w(tag, message);
		}
	}
	
	public static void e(String tag, String message) {
		if (DebugInterface.DEBUG) {
			Log.e(tag, message);
		}
	}
	
	public static void stackTrace(String tag, Throwable e) {
		if (DebugInterface.DEBUG) {
			e.printStackTrace();
		}
	}
	
}
