package de.eos.uptrade.abfahrtsmonitor.gson;

import java.io.Reader;

import com.google.gson.Gson;

public class GsonManager {

	public static final String TAG = GsonManager.class.getSimpleName();

	private static GsonManager sInstance = null;
	private Gson mGson;

	private GsonManager() {
		mGson = new Gson();
	}

	public static GsonManager getInstance() {
		if (sInstance == null) {
			sInstance = new GsonManager();
		}

		return sInstance;
	}

	public String toJson(Object src) {
		return getGson().toJson(src);
	}

	public <T> T fromJson(Reader json, Class<T> classOfT) {
		return getGson().fromJson(json, classOfT);
	}

	public <T> T fromJson(String json, Class<T> classOfT) {
		return getGson().fromJson(json, classOfT);
	}

	public Gson getGson() {
		return mGson;
	}

}