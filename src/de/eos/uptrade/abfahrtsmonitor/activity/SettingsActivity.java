package de.eos.uptrade.abfahrtsmonitor.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import de.eos.uptrade.abfahrtsmonitor.R;
import de.eos.uptrade.abfahrtsmonitor.exception.HttpRequestException;
import de.eos.uptrade.abfahrtsmonitor.gson.GsonManager;
import de.eos.uptrade.abfahrtsmonitor.interfaces.ConfigInterface;
import de.eos.uptrade.abfahrtsmonitor.model.DateTime;
import de.eos.uptrade.abfahrtsmonitor.model.FiffyException;
import de.eos.uptrade.abfahrtsmonitor.model.Point;
import de.eos.uptrade.abfahrtsmonitor.model.PointVerification;
import de.eos.uptrade.abfahrtsmonitor.request.PointVerificationRequest;
import de.eos.uptrade.abfahrtsmonitor.request.RequestHolder;
import de.eos.uptrade.abfahrtsmonitor.utils.Connectivity;
import de.eos.uptrade.abfahrtsmonitor.utils.DeviceInfo;

public class SettingsActivity extends Activity implements OnItemClickListener,
		OnClickListener {

	private EditText mSearchField;
	private ImageButton mSearchButton;
	private PointListAdapter mPointListAdapter;
	private PointVerification mPointVerification;
	private View mSearchProgress;

	/**
	 * Layout in die Activity inflaten.
	 * 
	 * Schriftart aus den Assets laden.
	 * 
	 * ListenAdapter intialisieren, dieser wird genutzt um die Liste mit Daten
	 * der Haltestellen zu f�llen.
	 * 
	 * Ein Click-Listener den Suchknopf zuweisen.
	 * 
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Layout der Activity zuweisen
		setContentView(R.layout.activity_settings);

		// Listadapte instanzieren
		mPointListAdapter = new PointListAdapter(this,
				R.layout.point_listview_row, new ArrayList<Point>());
		// Liste aus dem Layout holen
		ListView lv = (ListView) findViewById(R.id.result_listview);
		// Der Listview den Listadapter �begergeben
		lv.setAdapter(mPointListAdapter);
		// Ein ClickListener f�r die Liste setzen
		lv.setOnItemClickListener(this);

		// Hole das Input-Suchfeld
		mSearchField = (EditText) findViewById(R.id.search_field);

		// Wenn das Textfeld den Fokus bekommt, �ffne die Tastatur
		InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputMethodManager.showSoftInput(mSearchField,
				InputMethodManager.SHOW_IMPLICIT);

		// Hole den Suchknopf
		mSearchButton = (ImageButton) findViewById(R.id.search_button);

		// Setze den Click-Listener auf den Suchknopf
		mSearchButton.setOnClickListener(this);

		mSearchProgress = findViewById(R.id.search_progress);
	}

	/**
	 * Wenn ein Item aus der Liste angeklickt wurde, beende die Activity und
	 * �bergebe das Item der MainActivity
	 */
	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int position,
			long id) {

		Point point = (Point) adapter.getItemAtPosition(position);
		if (point != null) {
			finishActivityForResult(point);
		}
	}

	/**
	 * Es werden die Daten von der Haltestelle ausgelesen und in einem Intent
	 * gespeichert. Dieser Intente wird als Result an die MainActivity
	 * zur�ckgegeben
	 * 
	 * @param point
	 *            Ausgew�hlte Haltestelle aus der Liste
	 */
	private void finishActivityForResult(Point point) {

		Intent intent = new Intent();
		intent.putExtra(ConfigInterface.INTENT_POINT_ID, point.getPointId());
		intent.putExtra(ConfigInterface.INTENT_POINT_NAME, point.getName());

		setResult(RESULT_OK, intent);
		finish();
	}

	/**
	 * Wenn ein ClickEvent ausgef�hrt wird, wird diese Methode aufgerufen und
	 * die View die das Event ausgel�st hat wird �bergeben
	 * 
	 * @param view
	 *            Die View die das Event ausgel�st hat
	 */
	@Override
	public void onClick(View view) {

		switch (view.getId()) {
		case R.id.search_button:

			// wenn der Suchknopfgedr�ckt wurde
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(mSearchField.getWindowToken(), 0);

			// alte Sucheregebnisse aus der Liste l�schen
			mPointListAdapter.clear();
			mPointListAdapter.notifyDataSetChanged();

			// Neue suche durchf�hren mit den Parametern aus dem Textfeld
			new SearchPointsTask().execute(mSearchField.getText().toString());
			break;
		}
	}

	/**
	 * Asynchroner Task der f�r die Sucher der Haltestellen zust�ndig ist.
	 * Dieser Task wird in einem eigenen Thread parallel zu dem UI-Thread
	 * ausgef�hrt
	 */
	private class SearchPointsTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Bevor der Task startet, setze die Progressbar aus visible
			mSearchProgress.setVisibility(View.VISIBLE);
		}

		/**
		 * Diese Methode wird in einem eigenen Thread automatisch ausgef�hrt
		 * sobald der Task gestartet wurde, Es wird ein neuer Point erstellt f�r
		 * die Suchanfrage. Dieser Point wird der PointVerification �bergeben.
		 * Es wird ein neuer Request erstellt der mit der PointVerification
		 * instanziert wird. Der Request wird ausgef�hrt und die Response wird
		 * geholt.
		 */
		@Override
		protected String doInBackground(String... params) {

			// Neuen Point mit dem Suchparameter vom Textfeld erstellen
			Point point = new Point(null, params[0], null);

			DateTime dt = new DateTime(getApplicationContext());
			// Neue PointVerificaiton erstellen
			mPointVerification = new PointVerification(point, dt);

			// HaltestellenSuche-Request instanzieren
			PointVerificationRequest request = new PointVerificationRequest(
					mPointVerification);
			
			// UID im Request-Header setzen um in den Logs die Request von dem
			// Ger�t tracen zu k�nnnen
			request.setHeader("X-Uniqueidentifier",
					DeviceInfo.getUniqueId(SettingsActivity.this));

			try {
				if(Connectivity.isOnline(SettingsActivity.this)) {
					// Request wird ausgef�hrt
					request.doRequest();
					
					try {
						// response holen
						return request.getResponse();
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
			} catch (HttpRequestException e) {

				e.printStackTrace();
			}

			return "";
		}

		/**
		 * Nach dem doInBackground() ausgef�hrt wurde, wird diese Methode
		 * aufgerufen. Als Parameter wird die Response �bergeben
		 * 
		 * @param result
		 *            Response vom request
		 */
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			// Verstecke die ProgressBar
			mSearchProgress.setVisibility(View.GONE);
			
			RequestHolder rh = null;
			
			if(!result.equals("")) {
				try {
					// Konvertiere die Response in ein Java-Objekt
					rh = GsonManager.getInstance().fromJson(result,
							RequestHolder.class);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			

			FiffyException fiffyException = null;

			if (rh != null) {
				// wenn der requestholder leer ist
				// hole die exception, warum er nicht durchgef�hrt werden konnte
				mPointVerification = rh.getPointVerification();
				fiffyException = rh.getException();
			}

			if (fiffyException != null) {
				// wenn eine Exception vorahnden ist
			}

			if (mPointVerification != null) {
				// wenn die sucher erfolgreich war
				if (mPointVerification.getVerifiedPoints().size() > 0) {

					// leere den Listadapter von alten ergebnissen
					mPointListAdapter.clear();
					// F�ge alle neuen Ergebnisse zum ListAdapter hinzu
					mPointListAdapter.addAll(mPointVerification
							.getVerifiedPoints());
					// Zeige Ergebnisse an
					mPointListAdapter.notifyDataSetChanged();
				} else if (mPointVerification.getPoint().getPointId() != null) {
					// wenn nur ein point gefunden wurde gebe ihn direkt an die
					// MainActivity zur�ck ohne die Liste der Points anzuzeigen
					finishActivityForResult(mPointVerification.getPoint());
				}

			}

		}

	}

}
