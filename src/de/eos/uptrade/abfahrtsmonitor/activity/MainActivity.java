package de.eos.uptrade.abfahrtsmonitor.activity;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;
import de.eos.uptrade.abfahrtsmonitor.R;
import de.eos.uptrade.abfahrtsmonitor.debug.LogCat;
import de.eos.uptrade.abfahrtsmonitor.exception.HttpRequestException;
import de.eos.uptrade.abfahrtsmonitor.gson.GsonManager;
import de.eos.uptrade.abfahrtsmonitor.interfaces.ConfigInterface;
import de.eos.uptrade.abfahrtsmonitor.interfaces.FahrinfoInterface;
import de.eos.uptrade.abfahrtsmonitor.model.DateTime;
import de.eos.uptrade.abfahrtsmonitor.model.FiffyException;
import de.eos.uptrade.abfahrtsmonitor.model.Point;
import de.eos.uptrade.abfahrtsmonitor.model.StationEvent;
import de.eos.uptrade.abfahrtsmonitor.model.StationMonitor;
import de.eos.uptrade.abfahrtsmonitor.request.DepartureMonitorRequest;
import de.eos.uptrade.abfahrtsmonitor.request.RequestHolder;
import de.eos.uptrade.abfahrtsmonitor.utils.Connectivity;
import de.eos.uptrade.abfahrtsmonitor.utils.DeviceInfo;
import de.eos.uptrade.abfahrtsmonitor.utils.StringUtils;

public class MainActivity extends Activity {

	public final static String TAG = MainActivity.class.getSimpleName();

	private Point mPoint;
	private TextView haltestelle, uhrzeit, emptyMessage;
	private Typeface typeface;
	private ListView mListView;
	private DepartureListAdapter mDepartureListAdapter;
	private StationMonitor mStationMonitor;
	private ScheduledExecutorService scheduleTaskExecutor = Executors
			.newScheduledThreadPool(1);

	/**
	 * Auslesen der SharedPreferences und danach �berpr�fen ob ein Point
	 * (Haltestelle) gefunden wurde. Wenn keine Point vorhanden ist, wird die
	 * SettingsActivity gestartet.
	 * 
	 * Layout in die Activity inflaten.
	 * 
	 * Schriftart aus den Assets laden.
	 * 
	 * ListenAdapter intialisieren, dieser wird genutzt um die Liste mit Daten
	 * der Abfahrtszeiten zu f�llen.
	 * 
	 * Die Schriftart den einzelnen Textfeldern zuweisen.
	 * 
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// ListAdapter f�r die Liste der Abfahrtszeiten erstellen
		mDepartureListAdapter = new DepartureListAdapter(this,
				R.layout.departure_listview_row, new ArrayList<StationEvent>());

		// Wenn keine Haltestelle vorhanden ist
		if (mPoint == null) {
			// Auslesen der SharedPreferences
			loadSharedPreferences();
		}

		// Wenn keine Haltestelle vorhanden ist
		if (mPoint == null) {
			// Starten der SettingsActivity um eine Haltestelle zu w�hlen
			startSettingsActivityForResult();
		}

		// Layout in die Activity inflaten
		setContentView(R.layout.activity_main);

		// Laden der Schriftart
		typeface = Typeface.createFromAsset(getAssets(), "fonts/basicdots.ttf");

		// Liste f�r die Abfahrtszeiten aus dem Layout anhand der ID suchen
		mListView = (ListView) findViewById(R.id.listview);

		// Den Adapter der List �bergeben
		mListView.setAdapter(mDepartureListAdapter);
		// Einstellungen f�r die Liste durchf�hren
		mListView.setDivider(null);
		mListView.setDividerHeight(15);
		mListView.setClickable(false);

		// Suche die Textfelder in den Layouts
		haltestelle = (TextView) findViewById(R.id.haltestelle);
		uhrzeit = (TextView) findViewById(R.id.uhrzeit);
		emptyMessage = (TextView) findViewById(R.id.empty_message);

		// Schriftart festlegen
		haltestelle.setTypeface(typeface);
		// Horizontales scrollen des Haltestellenname, wenn dieser zu lang ist
		haltestelle.setSelected(true);
		haltestelle.setMovementMethod(new ScrollingMovementMethod());

		// Schriftart festlegen
		uhrzeit.setTypeface(typeface);
		emptyMessage.setTypeface(typeface);

		// Die Message bei keinen vorhanden Abfahrtszeiten setzen
		emptyMessage.setText(getString(R.string.empty_message));

		// Die EmptyView der Liste �bergeben, diese wird angezeigt wenn
		// der ListAdapter keine Eintr�ge aufweisst.
		mListView.setEmptyView(emptyMessage);

		// Wenn eine Haltestelle vorhanden ist, starte den Aktualisierungstask
		if (mPoint != null) {
			initUpdate();
		}
	}

	/**
	 * Auslesen der SharedPreference, dabei wird geschaut ob ein Point
	 * (Haltestelle) vorhanden ist. Wenn ein Point ausgelesen werden konnte,
	 * wird dieser in die Instanzvariable mPoint gesetzt.
	 * 
	 */
	private void loadSharedPreferences() {

		SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);

		String point_id = sharedPreferences.getString(
				ConfigInterface.SHARED_PREFERENCES_POINT_ID, "");
		String point_name = sharedPreferences.getString(
				ConfigInterface.SHARED_PREFERENCES_POINT_NAME, "");

		if (!point_id.equals("") && !point_name.equals("")) {
			mPoint = new Point(point_id, point_name, null);
		}

	}

	/**
	 * Speichern der SharedPreferences. Es wird der aktuelle Point (Haltestelle)
	 * ausgelesen und dieser dann in die SharedPreferences gespeichert.
	 */
	private void saveSharedPreferences() {
		if (mPoint != null) {
			SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
			SharedPreferences.Editor editor = sharedPreferences.edit();
			editor.putString(ConfigInterface.SHARED_PREFERENCES_POINT_ID,
					mPoint.getPointId());
			editor.putString(ConfigInterface.SHARED_PREFERENCES_POINT_NAME,
					mPoint.getName());
			editor.commit();
		}
	}

	/**
	 * Das MenuLayout wird inflatet.
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/**
	 * Diese Methode wird aufgerufen, wenn ein Item aus dem Menu angeklickt
	 * wurde
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		// Items die vorhanden sind mit der angeklickten Item ID vergleichen
		switch (item.getItemId()) {
		case R.id.action_settings:
			// Es wurde das Item Settings angeklickt, starten der
			// SettingsActivity
			startSettingsActivityForResult();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Wenn in der SettingsActitivty die Methode finish() @see
	 * {@link SettingsActivity#finishActivityForResult()} aufgerufen wird, wird
	 * als diese Methode aufgerufen. Es wird ein RequestCode, ResultCode und der
	 * Intent mit allen ben�tigten Daten f�r die ausgew�hlten Konfiguration
	 * �bergeben.
	 * 
	 * @param requestCode
	 *            Der Code der beim Starten der Actitivity �bergeben wurde @see
	 *            {@link MainActivity#startSettingsActivityForResult()}
	 * @param resultCode
	 *            Der Code der beim beenden der SettingsActitvity gesetzt wird @see
	 *            {@link SettingsActivity#finishActivityForResult()}
	 * @param data
	 *            Alle ben�tigten Konfigurationsdaten aus der SettingsActivity
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		// Wenn der Code f�r die Anfrage 200 ist.
		if (requestCode == 200) {
			// Wenn der ResultCode ok ist
			if (resultCode == RESULT_OK) {
				// erstelle einen neuen Point(Haltestelle) mit den Daten aus dem
				// Intent
				mPoint = new Point(
						data.getStringExtra(ConfigInterface.INTENT_POINT_ID),
						data.getStringExtra(ConfigInterface.INTENT_POINT_NAME),
						null);

				// Speicher den Point in die Shared-Preferences
				saveSharedPreferences();

				// Starte einen neuen Excecutor
				scheduleTaskExecutor = Executors.newScheduledThreadPool(1);
				// Starte das Update
				initUpdate();
			}
		}
	}

	/**
	 * Es wird ein Intent erstellt und damit die Settingsactivity gestartet.
	 * Bevor die Settingsactivity gestartet wird, wird der TaskExecutor
	 * heruntergefahren und alle Tasks die noch ausgef�hrt werden m�ssen
	 * abgebrochen. Es wird auch die Liste f�r die Abfahrten geleert.
	 */
	private void startSettingsActivityForResult() {
		// TaskExecutor herunterfahren
		scheduleTaskExecutor.shutdownNow();
		// Intent f�r die SettingsActivity erstellen
		Intent intent = new Intent(this, SettingsActivity.class);
		// Settingsactivity starten
		startActivityForResult(intent, 200);
		// Liste leeren
		mDepartureListAdapter.clear();
		// Sage der Liste, dass sich was ge�ndert hat
		mDepartureListAdapter.notifyDataSetChanged();
	}

	/**
	 * Ein asynchroner Task wo der Request f�r die Abfahrtszeiten zu einer
	 * bestimmten Haltestelle gemacht wird.
	 * 
	 * @author msangals
	 */
	private class LoadDeparturesTask extends AsyncTask<String, Void, String> {

		/**
		 * Diese Methode wird im Hintergrund automatisch aufgerufen. Es wird der
		 * Request an den FahrInfo-Server gemacht mit einen aktuell vorhandenen
		 * Point.
		 * 
		 * @return Die Antwort vom Request als JSON-String
		 */
		@Override
		protected String doInBackground(String... params) {

			LogCat.d(TAG, "doInBackground()");

			// Datum f�r die gew�nschte abfahrtszeit
			DateTime dt = new DateTime(getApplicationContext());

			// Abfahrtsmonitor wo alle Abfahrten und weitere Informationen
			// gehalten werden
			StationMonitor sm = new StationMonitor(mPoint, dt);

			// Die Request klasse f�r den Abfarhtsmonitor-Request
			DepartureMonitorRequest request = new DepartureMonitorRequest(sm);

			// Eine eindeutige ID vom Client wird mit in den Request gesetzt.
			request.setHeader("X-Uniqueidentifier",
					DeviceInfo.getUniqueId(MainActivity.this));

			try {

				if (Connectivity.isOnline(MainActivity.this)) {
					LogCat.d(TAG, "connectivity is Online");
					// Request wird ausgef�hrt
					request.doRequest();

					try {
						// Die Antwort als JSON-String vom Request wird geholt
						return request.getResponse();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} catch (HttpRequestException e) {
				e.printStackTrace();
			}

			return "";
		}

		/**
		 * Diese Methode wird automatisch nach der @see
		 * {@link LoadDeparturesTask#doInBackground()} ausgef�hrt. Es wird als
		 * Parameter die Antwort vom Request �bergeben. Dieser Request von JSON
		 * zum Java Objekt geparsed und weiterverarbeitet. Es werden alle
		 * ben�tigten Daten ausgelesen und auf dem Abfahrtsmonitor angezeigt.
		 * 
		 * @param result
		 *            Ist die Antwort vom Request als JSON-String
		 */
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			RequestHolder rh = null;

			if (!result.equals("")) {

				try {
					// Die Antwort wird �ber GSON aus einem JSON-String zu einem
					// Java-Objekt geparsed
					rh = GsonManager.getInstance().fromJson(result,
							RequestHolder.class);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (rh == null) {
				
				sendMessageToHandler(getString(R.string.empty_message));
				// Leere die Liste der Abfahrten
				mDepartureListAdapter.clear();
				// Sage der Liste, dass sich was ge�ndert hat
				mDepartureListAdapter.notifyDataSetChanged();

				return;
			}

			
			
			// Hole den Abfahrtsmonitor aus dem Antwortsholder
			mStationMonitor = rh.getStationMonitor();

			// Exception
			FiffyException fiffyException = null;
			// hole die exception aus dem Antwortsholder
			fiffyException = rh.getException();

			// wenn eine Exception vorhanden ist
			if (fiffyException != null) {

				sendMessageToHandler(fiffyException.getPublicMessage());

				// Leere die Abfahrtsmonitorliste
				mDepartureListAdapter.clear();
				// Sage der Liste, dass sich was ge�ndert hat
				mDepartureListAdapter.notifyDataSetChanged();

				// Wenn keine Exception vorhanden ist und ein
				// Abfahrtsmonitor da ist und die Abfahrten gr��er als 0
				// sind
			} else if (mStationMonitor != null
					&& mStationMonitor.getStationEvents().size() > 0) {

				// Leere die Liste der Abfahrten
				mDepartureListAdapter.clear();
				// Setze die neuen Abfahrten aus dem Abfahrtsmonitor in die
				// liste
				mDepartureListAdapter
						.addAll(mStationMonitor.getStationEvents());
				// Sage der Liste, dass sich was ge�ndert hat
				mDepartureListAdapter.notifyDataSetChanged();
			}

		}
	}
	
	private void sendMessageToHandler(String msg) {
		// Hole die message f�r den Handler
		Message message = handler.obtainMessage();
		// Message code
		message.what = 404;
		// Schreibe die Public Message von der Exception in die
		// Message f�r den Handler
		message.obj = msg;
		// �bergebe die Message an den Handler
		handler.sendMessage(message);
	}
	
	/**
	 * Es wird das Update der Abfahrten f�r eine bestimmte Haltestelle
	 * gestartet.
	 */
	private void initUpdate() {

		// Setze den Haltenstellennamen in das Textfeld f�r die Haltestelle
		haltestelle.setText(StringUtils.replaceUmlaut(mPoint.getName()));

		// Der Taskexecuter nimmt den Task auf und f�r diesen in einer fest
		// gesetzten Zeit
		// aus. Die Zeiten sind im Interface definiert.
		scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {

			@Override
			public void run() {
				// Der Asynchrone Task f�r das Anfragen der Abfahrtzeiten wird
				// ausgef�hrt
				new LoadDeparturesTask().execute();
			}

		}, 0, FahrinfoInterface.FIFFY_REQUEST_TIME, TimeUnit.SECONDS);
	}

	/**
	 * Handler f�r das Updaten der UI-Kompontenten. Dieser Handler l�uft im
	 * Main-UI-Thread. Dieser wird benutzt um die Meldungen von einer Exception
	 * im Abfahrtsmonitor anzeigen zu lassen.
	 */
	final Handler handler = new Handler() {

		/**
		 * Wenn �ber den Handler die Methode sendMessage() aufgerufen wird, wird
		 * diese Methode automatisch aufgerufen. Hier wird die Message als
		 * Parameter �bergeben.
		 * 
		 * @param msg
		 *            Message mit der Meldung von der Exception
		 */
		@Override
		public void handleMessage(Message msg) {

			// Wenn der MessageCode 404 ist
			if (msg.what == 404) {
				// Setze die Exception-Message in die daf�r zust�ndige View
				emptyMessage.setText(StringUtils
						.replaceUmlaut((String) msg.obj));
			}
			super.handleMessage(msg);
		}
	};

}
