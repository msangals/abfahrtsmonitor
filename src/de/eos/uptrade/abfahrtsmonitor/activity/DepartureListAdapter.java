package de.eos.uptrade.abfahrtsmonitor.activity;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import de.eos.uptrade.abfahrtsmonitor.R;
import de.eos.uptrade.abfahrtsmonitor.model.DateTime;
import de.eos.uptrade.abfahrtsmonitor.model.StationEvent;
import de.eos.uptrade.abfahrtsmonitor.model.VehicleEvent;
import de.eos.uptrade.abfahrtsmonitor.model.VehicleRoute;
import de.eos.uptrade.abfahrtsmonitor.utils.StringUtils;

public class DepartureListAdapter extends ArrayAdapter<StationEvent> {

	private Context mContext;
	private int mResourceId;

	List<StationEvent> stationEvents = new ArrayList<StationEvent>();

	/**
	 * Konstruktor f�r den ArrayAdapter
	 * 
	 * @param context
	 *            Der Context von der App
	 * @param layoutResourceId
	 *            Die ID vom Zeilenlayout
	 * @param stationEvents
	 *            Eine liste aller Abfahrten
	 */
	public DepartureListAdapter(Context context, int layoutResourceId,
			List<StationEvent> stationEvents) {
		super(context, layoutResourceId, stationEvents);
		this.mContext = context;
		this.mResourceId = layoutResourceId;
		this.stationEvents = stationEvents;
	}

	/**
	 * Diese Methode wird automatisch beim erstellen der Views innerhalb der
	 * Liste aufgerufen.
	 * 
	 * @param position
	 *            Die aktuelle Position des Items innerhalb der Liste
	 * @param convertView
	 *            Die alte View zum wiederverwenden.
	 * @param parent
	 *            Die Eltern-View von der converView
	 * 
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder viewHolder;

		// Es wird gepr�ft ob ein View schon exisitiert
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(mResourceId, parent, false);

			viewHolder = new ViewHolder();

			// Hole die Schriftart
			Typeface tf = Typeface.createFromAsset(mContext.getAssets(),
					"fonts/basicdots.ttf");

			// Suche die Views anhand der ID im Layout
			viewHolder.linie = (TextView) convertView.findViewById(R.id.linie);
			viewHolder.ziel = (TextView) convertView.findViewById(R.id.ziel);
			viewHolder.abfahrt = (TextView) convertView
					.findViewById(R.id.abfahrt);

			// Setze die Schriftart bei den Views
			viewHolder.linie.setTypeface(tf);
			viewHolder.ziel.setTypeface(tf);
			viewHolder.abfahrt.setTypeface(tf);

			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		// Hole das Element von den StationEvent aus dem ArrayAdapter mit der
		// aktuellen position
		StationEvent se = getItem(position);

		//Informationen zur abfahrt
		VehicleEvent ve = se.getVehicleEvent();
		//Die Route von der Abfahrt
		VehicleRoute vh = se.getRoute();

		//Setze die Daten von der Route in die Tectfelder
		viewHolder.linie.setText(vh.getLine().getName());
		viewHolder.ziel.setText(StringUtils.replaceUmlaut(vh.getLine()
				.getTerminus().getName()));
		DateTime dateTime = (ve.getExpectedDatetime() != null) ? ve
				.getExpectedDatetime() : ve.getScheduledDatetime();
		DateTime dt = new DateTime(mContext, dateTime.getDateTime());
		viewHolder.abfahrt.setText(dt.getFormattedDepartureTime());

		return convertView;
	}

	/**
	 * Diese Klasse cached die Views um diese nicht bei jeden aufruf der Liste
	 * neu zu laden
	 */
	private static class ViewHolder {
		// Views die gecached werden
		TextView linie, ziel, abfahrt;
	}
}
