package de.eos.uptrade.abfahrtsmonitor.activity;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import de.eos.uptrade.abfahrtsmonitor.R;
import de.eos.uptrade.abfahrtsmonitor.model.Point;

public class PointListAdapter extends ArrayAdapter<Point> {

	private Context mContext;
	private int mResourceId;

	List<Point> mPoints = new ArrayList<Point>();

	/**
	 * Konstruktor f�r den ArrayAdapter
	 * 
	 * @param context
	 *            Der Context von der App
	 * @param layoutResourceId
	 *            Die ID vom Zeilenlayout
	 * @param stationEvents
	 *            Eine liste aller Haltestellen
	 */
	public PointListAdapter(Context context, int layoutResourceId,
			List<Point> points) {
		super(context, layoutResourceId, points);

		this.mContext = context;
		this.mResourceId = layoutResourceId;
		this.mPoints = points;
	}

	/**
	 * Diese Methode wird automatisch beim erstellen der Views innerhalb der
	 * Liste aufgerufen.
	 * 
	 * @param position
	 *            Die aktuelle Position des Items innerhalb der Liste
	 * @param convertView
	 *            Die alte View zum wiederverwenden.
	 * @param parent
	 *            Die Eltern-View von der converView
	 * 
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder viewHolder;

		// Es wird gepr�ft ob ein View schon exisitiert
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(mResourceId, parent, false);

			viewHolder = new ViewHolder();

			viewHolder.name = (TextView) convertView
					.findViewById(R.id.point_name);

			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		// Hole die Haltestelle aus dem ArrayAdapter mit der aktuellen Position
		Point point = getItem(position);

		//Setze den Namen der Haltestelle in das Textfeld
		viewHolder.name.setText(point.getName());

		return convertView;
	}

	/**
	 * Diese Klasse cached die Views um diese nicht bei jeden aufruf der Liste
	 * neu zu laden
	 */
	private static class ViewHolder {
		TextView name;
	}

}
