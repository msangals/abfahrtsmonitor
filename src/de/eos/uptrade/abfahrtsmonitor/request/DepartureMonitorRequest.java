package de.eos.uptrade.abfahrtsmonitor.request;

import de.eos.uptrade.abfahrtsmonitor.exception.HttpRequestException;
import de.eos.uptrade.abfahrtsmonitor.gson.GsonManager;
import de.eos.uptrade.abfahrtsmonitor.interfaces.FahrinfoInterface;
import de.eos.uptrade.abfahrtsmonitor.model.StationMonitor;

public class DepartureMonitorRequest extends HttpRequest {

	public final static String TAG = DepartureMonitorRequest.class
			.getSimpleName();

	private StationMonitor mStationMonitor;

	public DepartureMonitorRequest(StationMonitor stationMonitor) {
		super(FahrinfoInterface.FIFFY_HOST,
				FahrinfoInterface.FIFFY_STATION_MONITOR_URL_PATH, "POST");

		mStationMonitor = stationMonitor;
	}

	@Override
	public void doRequest() throws HttpRequestException {

		RequestHolder rh = new RequestHolder();
		rh.setStationMonitor(mStationMonitor);
		
		final String json = GsonManager.getInstance().toJson(rh);
		
		setPostData(json);
		setHmacSignature(mPostData, FahrinfoInterface.FIFFY_SALT);
		setHeader("X-ApplicationKey", FahrinfoInterface.FIFFY_APPLICATION_KEY);
		
		mStationMonitor = null;
		
		super.doRequest();		
	}

}
