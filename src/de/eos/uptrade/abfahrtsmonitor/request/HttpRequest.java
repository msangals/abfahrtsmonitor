package de.eos.uptrade.abfahrtsmonitor.request;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HttpsURLConnection;

import de.eos.uptrade.abfahrtsmonitor.activity.MainActivity;
import de.eos.uptrade.abfahrtsmonitor.debug.LogCat;
import de.eos.uptrade.abfahrtsmonitor.exception.HttpRequestException;
import de.eos.uptrade.abfahrtsmonitor.utils.Base64;
import de.eos.uptrade.abfahrtsmonitor.utils.Connectivity;
import de.eos.uptrade.abfahrtsmonitor.utils.StringUtils;

/**
 * HttpRequest Generelle Klasse zum Ausfuehren jeglicher Requests zum
 * FahrInfo-Server
 * 
 */
public class HttpRequest {

	public static final String TAG = HttpRequest.class.getSimpleName();

	public static final String PROTOCOL_HTTPS = "https";
	public static final String PROTOCOL_HTTP = "http";

	public static final int TIMEOUT = 30000;

	protected String mProtocol;
	protected String mHost;
	protected String mPath;
	protected String mMethod;
	protected int mTimeout;

	protected String mPostData;
	protected HashMap<String, String> mHeaders;

	protected String mResponse;
	protected InputStream mResponseStream;
	protected int mResponseCode;
	protected String mResponseContentType;
	protected String mResponseCharset;
	protected Map<String, List<String>> mHeaderFields;

	protected HttpURLConnection mConnection;

	/**
	 * Constructor
	 * 
	 * @param host
	 *            Host des Servers google.de
	 * @param path
	 *            Pfad auf dem Server beginneng mit "/" z.B.
	 *            /index.php/mobileService/sync
	 * @param method
	 *            Request-Method der Anfrage GET oder POST
	 */
	public HttpRequest(String host, String path, String method) {

		setProtocol(HttpRequest.PROTOCOL_HTTPS);
		setHost(host);
		setPath(path);
		setMethod(method);
		setTimeout(HttpRequest.TIMEOUT);

		// Postdata for the request
		mPostData = "";

		// Set the Header for the request
		mHeaders = new HashMap<String, String>();
		mHeaders.put("Accept-Language", getLangCode());

		this.mResponse = "";
		this.mResponseStream = null;
		this.mResponseCode = -1;
		this.mResponseContentType = null;
		this.mResponseCharset = null;

		this.mConnection = null;
	}

	/**
	 * F�hrt den Request aus
	 * 
	 * @throws HttpRequestException
	 */
	public void doRequest() throws HttpRequestException {
		
		try {

			String uri = mProtocol + "://" + mHost + mPath;
			
			//Verbindung aufbauen
			openConnection(uri);
			
			// Header setzen
			for (String key : mHeaders.keySet()) {
				mConnection.setRequestProperty(key, mHeaders.get(key));
			}

			// Falls es ein POST-Request ist, OutputStream erzeugen, Header
			// setzen, etc.
			if (mMethod.compareTo("POST") == 0) {
				
				DataOutputStream output = null;

				try {
//					LogCat.v(TAG, "PostData: " + mPostData);
					mConnection.setRequestMethod(mMethod);
					mConnection.setDoOutput(true);
					mConnection.setRequestProperty("Content-Type",
							"application/json; charset=utf-8");
					
//					LogCat.d(TAG, "method: " + mConnection.getRequestProperties());
					
					try {
						// Postdaten senden
						output = new DataOutputStream(mConnection.getOutputStream());
//						LogCat.d(TAG, "Outputsize " + output.size());
					} catch (IOException e) {
						// TODO: handle exception
						LogCat.d(TAG, "DataOutputError " + e.getMessage());
						
					}
					
					Writer writer = new OutputStreamWriter(
							mConnection.getOutputStream(), "UTF-8");
					writer.write(mPostData);
					writer.close();
				} catch (Exception e) {

					

					throw new HttpRequestException(
							"POST-Daten konnten nicht gesendet werden", e);
				} finally {
					if (output != null) {
						try {
							output.close();
						} catch (IOException e) {
						} finally {
							output = null;
						}
					}
				}
			}

			// ResponseCode und HeaderFields auslesen
			mResponseCode = mConnection.getResponseCode();
			mHeaderFields = mConnection.getHeaderFields();
			
			
			// Content-type und charset
			mResponseContentType = mConnection.getHeaderField("Content-Type");

			if (mResponseContentType != null) {
				this.mResponseCharset = null;
				for (String param : this.mResponseContentType.replace(" ", "")
						.split(";")) {
					if (param.startsWith("charset=")) {
						this.mResponseCharset = param.split("=", 2)[1];
						break;
					}
				}
			} else {
				LogCat.w(TAG, "doRequest: no content was returned");
			}

			mResponse = "";
			if (mResponseStream != null)
				try {
					this.mResponseStream.close();
				} catch (IOException e) {
				}
			mResponseStream = null;
			
			LogCat.d(TAG, "code: " + mResponseCode);
			
			if (mResponseCode == 200) {
				mResponseStream = mConnection.getInputStream();
				// Anfrage erfolgreich
			} else {
				mResponseStream = mConnection.getErrorStream();
			}

		} catch (Exception e) {
			LogCat.d(TAG, "doRequest: request failed");
			LogCat.stackTrace(TAG, e);

		} finally {
			// this.connection.disconnect();
			// this.connection = null;
		}

	}

	private String readInputStream() throws HttpRequestException {
		String line;
		StringBuilder input = new StringBuilder();

		try {
			if (mResponseCharset != null) {

				InputStreamReader inputStreamReader = null;
				BufferedReader bufferReader = null;
				try {
					inputStreamReader = new InputStreamReader(mResponseStream,
							this.mResponseCharset);
					bufferReader = new BufferedReader(inputStreamReader);
					while ((line = bufferReader.readLine()) != null) {
						input.append(line);
					}
				} finally {
					if (inputStreamReader != null)
						try {
							inputStreamReader.close();
						} catch (IOException logOrIgnore) {
						}
					if (bufferReader != null)
						try {
							bufferReader.close();
						} catch (IOException logOrIgnore) {
						}
				}
			} else {

				// Eigentlich das gleiche wie oben, nur ohne Charsetangabe
				InputStreamReader inputStreamReader = null;
				BufferedReader bufferReader = null;
				try {
					
					if(mResponseStream != null) {
						LogCat.d(TAG, "mResponseStream ist nicht leer");						
					}
					else {
						LogCat.d(TAG, "mResponseStream ist null");						
					}
					
					
					inputStreamReader = new InputStreamReader(this.mResponseStream);
					bufferReader = new BufferedReader(inputStreamReader);
					while ((line = bufferReader.readLine()) != null) {
						input.append(line);
					}
				} finally {
					if (inputStreamReader != null)
						try {
							inputStreamReader.close();
						} catch (IOException logOrIgnore) {
						}
					if (bufferReader != null)
						try {
							bufferReader.close();
						} catch (IOException logOrIgnore) {
						}
				}
			}

		} catch (IOException e) {
			throw new HttpRequestException(
					"InputStream konnte nicht in einen String eingelesen werden",
					e);
		}

		return input.toString();
	}
	
	/**
	 * �ffnet eine URL connection mit der �bergeben uri
	 * 
	 * @param uri uri als string wohin die verbindung aufgebaut werden soll
	 * @throws Exception
	 */
	private void openConnection(String uri) throws Exception {

		System.setProperty("http.keepAlive", "false");

		URL url = new URL(uri);
		// URLConnection
		if (HttpRequest.PROTOCOL_HTTPS.equalsIgnoreCase(mProtocol)) {
			this.mConnection = (HttpsURLConnection) url.openConnection();
		} else {
			this.mConnection = (HttpURLConnection) url.openConnection();
		}

		// Connection konfigurieren
		this.mConnection.setDoInput(true);
		this.mConnection.setInstanceFollowRedirects(true);
		this.mConnection.setConnectTimeout(mTimeout);
		this.mConnection.setUseCaches(false);

	}

	/**
	 * Gibt den aktuellen LangCode vom Device zur�ck
	 * 
	 * @return String langcode (z.B. DE, EN, US)
	 */
	private static String getLangCode() {
		final Locale lang = Locale.getDefault();
		final String country = lang.getCountry();
		final String langcode;
		if (country.length() > 0) {
			langcode = MessageFormat.format("{0}-{1}", lang.getLanguage(),
					country);
		} else {
			langcode = lang.getLanguage();
		}
		return langcode;
	}

	/**
	 * Liefert den Response des Servers zurueck. Hierfuer sollte logischerweise
	 * zuvor doRequest ausgefuehrt werden
	 * 
	 * @return String mit dem Inhalt der Serverantwort
	 * @throws HttpRequestException
	 */
	public String getResponse() throws HttpRequestException {
		mResponse = readInputStream();
		if (mResponseStream != null)
			try {
				mResponseStream.close();
			} catch (IOException e) {
			}
		mResponseStream = null;

		if (mResponse == null) {
//			LogCat.d(TAG, "getResponse: length=null sample=null");
		} else {
			int length = mResponse.length();
//			LogCat.d(TAG, "getResponse: length=" + length + "B sample="
//					+ (length > 128 ? mResponse.substring(0, 128) : mResponse));
		}

		return mResponse;
	}

	/**
	 * Liefert den Response als String zur�ck
	 * 
	 * @return
	 */
	public InputStream getResponseStream() {
		return mResponseStream;
	}

	/**
	 * Liefert den ResponseCode der letzten Serverantwort -1, wenn Request
	 * fehlgeschlagen ist
	 * 
	 * @return int HTTP-ResponseCode
	 */
	public long getResponseCode() {
		return this.mResponseCode;
	}

	public void cancel() {
		if (mResponseStream != null) {
			try {
				mResponseStream.close();
			} catch (IOException e) {
			} finally {
				mResponseStream = null;
			}
		}

		try {
			mConnection.disconnect();
		} catch (Exception e) {
		} finally {
			mConnection = null;
		}

	}

	/**
	 * Liefert den kompletten Headerwert fuer einen Header zurueck
	 * 
	 * @param header
	 *            String Name des Headers z.B. cache-control
	 * @return Liste mit Strings, die diesem Header zugewiesen sind z.B.
	 *         [private, max-age=0]
	 * @throws eosHttpRequestException
	 */
	public List<String> getHeader(String header) throws HttpRequestException {
		if (!mHeaderFields.containsKey(header)) {
			throw new HttpRequestException("Der Header mit dem Namen '"
					+ header + "' existiert nicht!");
		}

		return mHeaderFields.get(header);
	}

	/**
	 * Liefert den geforderten Wert eines Headers
	 * 
	 * @param header
	 *            String Name des Headers z.B. cache-control
	 * @param position
	 *            int Position des Headerwertes beginnend bei 0
	 * @return Erster String des Headers z.B. bei [private, max-age=0] wueder
	 *         private zurueckgeliefert
	 * @throws fiHttpRequestException
	 */
	public String getValueOfHeader(String header, int position)
			throws HttpRequestException {
		if (getHeader(header).size() - 1 < position || position < 0) {
			throw new HttpRequestException("Der Header mit dem Namen '"
					+ header + "' besitzt keinen Wert an der Position: "
					+ position + "!");
		}

		return getHeader(header).get(position);
	}

	/**
	 * Liefert den ERSTEN Wert eines Headers
	 * 
	 * @param header
	 *            String Name des Headers z.B. cache-control
	 * @return Erster String des Headers z.B. bei [private, max-age=0] wueder
	 *         private zurueckgeliefert
	 * @throws fiHttpRequestException
	 */
	public String getFirstValueOfHeader(String header)
			throws HttpRequestException {
		return getValueOfHeader(header, 0);
	}

	/**
	 * Falls noch der InputStream offen war diesen schließen
	 * 
	 * @throws Throwable
	 */
	@Override
	public void finalize() throws Throwable {
		cancel();

		super.finalize();
	}

	public String getHost() {
		return mHost;
	}

	public void setHost(String host) {
		mHost = host;
	}

	public String getMethod() {
		return mMethod;
	}

	public void setMethod(String method) {
		mMethod = method;
	}

	public String getPath() {
		return mPath;
	}

	public void setPath(String path) {
		mPath = path;
	}

	public int getTimeout() {
		return mTimeout;
	}

	public void setTimeout(int timeout) {
		mTimeout = timeout;
	}

	public void setProtocol(String protocol) {
		if (HttpRequest.PROTOCOL_HTTPS.equalsIgnoreCase(protocol)) {
			mProtocol = HttpRequest.PROTOCOL_HTTPS;
		} else {
			mProtocol = HttpRequest.PROTOCOL_HTTP;
		}
	}

	public void setPostData(String postData) {
		mPostData = postData;
	}

	public void setAuth(String auth) {
		mHeaders.put("Authorization", "Basic " + auth);
	}

	public void setAuth(String username, String password) {
		String auth = username + ":" + password;

		setAuth(Base64.encodeBytes(auth.getBytes()));
	}

	public void setUserAgent(String userAgent) {
		mHeaders.put("User-Agent", userAgent);
	}

	public void setHeader(String name, String value) {
		mHeaders.put(name, value);
	}


	/**
	 * Setzt die HmacSignature f�r den Request. Die Signatur
	 * wird aus dem �bergeben Request und dem Salt generiert
	 * 
	 * @param request Der Requestbody (JSON-String)
	 * @param salt generierter salt von EOS-UPTRADE
	 */
	public void setHmacSignature(String request, String salt) {
		if (request == null || salt == null)
			return;

		try {
			byte[] key = salt.getBytes();
			String algorithm = "HmacSHA1";

			SecretKeySpec keySpec = new SecretKeySpec(key, algorithm);
			Mac mac = Mac.getInstance(algorithm);

			mac.init(keySpec);
			byte[] sigBytes = mac.doFinal(request.getBytes());
			String hex = StringUtils.getHexString(sigBytes);
			
			this.setHeader("X-HMAC-Signature", hex.toUpperCase());

		} catch (Exception e) {
			return;
		}
	}
	

}
