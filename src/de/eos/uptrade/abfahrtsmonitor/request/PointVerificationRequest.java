package de.eos.uptrade.abfahrtsmonitor.request;

import de.eos.uptrade.abfahrtsmonitor.exception.HttpRequestException;
import de.eos.uptrade.abfahrtsmonitor.gson.GsonManager;
import de.eos.uptrade.abfahrtsmonitor.interfaces.FahrinfoInterface;
import de.eos.uptrade.abfahrtsmonitor.model.PointVerification;

public class PointVerificationRequest extends HttpRequest {

	private PointVerification mPointVerification;
	
	public PointVerificationRequest(PointVerification pointVerification) {
		super(FahrinfoInterface.FIFFY_HOST,
				FahrinfoInterface.FIFFY_POINT_VERIFICATION_URL_PATH, "POST");
		
		mPointVerification = pointVerification;
	}

	@Override
	public void doRequest() throws HttpRequestException {

		RequestHolder rh = new RequestHolder();
		rh.setPointVerification(mPointVerification);
		
		final String json = GsonManager.getInstance().toJson(rh);
		
		setPostData(json);
		setHmacSignature(mPostData, FahrinfoInterface.FIFFY_SALT);
		setHeader("X-ApplicationKey", FahrinfoInterface.FIFFY_APPLICATION_KEY);
		
		mPointVerification = null;
		
		super.doRequest();		
	}
	
}
