package de.eos.uptrade.abfahrtsmonitor.request;

import com.google.gson.annotations.SerializedName;

import de.eos.uptrade.abfahrtsmonitor.model.FiffyException;
import de.eos.uptrade.abfahrtsmonitor.model.PointVerification;
import de.eos.uptrade.abfahrtsmonitor.model.StationMonitor;

public class RequestHolder {
	
	@SerializedName("stationMonitor")
	private StationMonitor mStationMonitor;
	
	@SerializedName("pointVerification")
	private PointVerification mPointVerification;
	
	@SerializedName("exception")
	private FiffyException mException;
	
	public RequestHolder() {}
	
	public StationMonitor getStationMonitor() {
		return mStationMonitor;
	}

	public void setStationMonitor(StationMonitor stationMonitor) {
		this.mStationMonitor = stationMonitor;
	}

	public PointVerification getPointVerification() {
		return mPointVerification;
	}

	public void setPointVerification(PointVerification pointVerification) {
		this.mPointVerification = pointVerification;
	}

	public FiffyException getException() {
		return mException;
	}

	public void setException(FiffyException exception) {
		this.mException = exception;
	}
}
