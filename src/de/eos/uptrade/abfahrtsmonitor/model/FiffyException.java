package de.eos.uptrade.abfahrtsmonitor.model;

public class FiffyException {

	private int code;
	private String message;
	private String technicalMessage;
	private String publicMessage;

	public FiffyException() {
	}

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public String getTechnicalMessage() {
		return technicalMessage;
	}

	public String getPublicMessage() {
		return publicMessage;
	}

	@Override
	public String toString() {
		return publicMessage + " -+- " + message;
	}

}
