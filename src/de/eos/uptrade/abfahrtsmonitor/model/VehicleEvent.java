package de.eos.uptrade.abfahrtsmonitor.model;

public class VehicleEvent {
	
	private Point point;
	private DateTime scheduledDatetime;
	private DateTime expectedDatetime;
	private DepartureArea area;
	
	public Point getPoint() {
		return point;
	}
	public void setPoint(Point point) {
		this.point = point;
	}
	public DateTime getScheduledDatetime() {
		return scheduledDatetime;
	}
	public void setScheduledDatetime(DateTime scheduledDatetime) {
		this.scheduledDatetime = scheduledDatetime;
	}
	public DateTime getExpectedDatetime() {
		return expectedDatetime;
	}
	public void setExpectedDatetime(DateTime expectedDatetime) {
		this.expectedDatetime = expectedDatetime;
	}
	public DepartureArea getArea() {
		return area;
	}
	public void setArea(DepartureArea area) {
		this.area = area;
	}
}
