package de.eos.uptrade.abfahrtsmonitor.model;

public class VehicleRoute {
	
	
	private String routeId;
	private Line line;
	private int type;
    private String actualVehicleType;
    private int actualVehicleCode;
	
	public String getRouteId() {
		return routeId;
	}
	public void setRouteId(String routeId) {
		this.routeId = routeId;
	}
	public Line getLine() {
		return line;
	}
	public void setLine(Line line) {
		this.line = line;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getActualVehicleType() {
		return actualVehicleType;
	}
	public void setActualVehicleType(String actualVehicleType) {
		this.actualVehicleType = actualVehicleType;
	}
	public int getActualVehicleCode() {
		return actualVehicleCode;
	}
	public void setActualVehicleCode(int actualVehicleCode) {
		this.actualVehicleCode = actualVehicleCode;
	}
	
	
}
