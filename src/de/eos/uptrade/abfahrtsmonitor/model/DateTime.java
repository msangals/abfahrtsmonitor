package de.eos.uptrade.abfahrtsmonitor.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import android.content.Context;

import com.google.gson.annotations.SerializedName;

import de.eos.uptrade.abfahrtsmonitor.R;
import de.eos.uptrade.abfahrtsmonitor.interfaces.FahrinfoInterface;

public class DateTime {

	@SerializedName("datetime")
	private String mDateTime;
	
	private transient Context mContext;
	private transient DateFormat mDfmt;
	
	private DateTime() {
		mDfmt = new SimpleDateFormat(FahrinfoInterface.FIFFY_TIME_FORMAT);
	}
	
	public DateTime(Context context) {
		this();
		mContext = context;
		setCurrentDatetime();
	}
	
	public DateTime(Context context, String dateTime) {
		this();
		mContext = context;
		mDateTime = dateTime;
	}
	
	private void setCurrentDatetime() {
		mDateTime = mDfmt.format(new Date());
	}
	
	private String getFormattedTime(String dateFormat, Date date) {
		DateFormat df = new SimpleDateFormat(dateFormat);
		return df.format(date);
	}
	
	public String getDateTime() {
		return mDateTime;
	}

	public void setDateTime(String dateTime) {
		this.mDateTime = dateTime;
	}

	public String getFormattedDepartureTime() {
		
		Date date = null;
		
		try {
			date = mDfmt.parse(mDateTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		if(date != null) {
			
			final int dateDiffSeconds = (int)getDateDiff(date, new Date(), TimeUnit.SECONDS);		
			
			if(dateDiffSeconds < FahrinfoInterface.FIFFY_DEPARTURE_TIME_LIMIT_LAST) {
				return mContext.getResources().getString(R.string.last_departure);
			} else if(dateDiffSeconds <= FahrinfoInterface.FIFFY_DEPARTURE_TIME_LIMIT) {
				
				return mContext.getResources().getString(R.string.departure_min, ((dateDiffSeconds + 30) / 60));
			} else {
				return getFormattedTime("HH:mm", date);
			}
		}
		
		return null;
	}
	
	/**
	 * Get a diff between two dates
	 * @param date1 the oldest date
	 * @param date2 the newest date
	 * @param timeUnit the unit in which you want the diff
	 * @return the diff value, in the provided unit
	 */
	private long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
		
	    long diff = date1.getTime() - date2.getTime();
	    return timeUnit.convert(diff, TimeUnit.MILLISECONDS);
	}
}
