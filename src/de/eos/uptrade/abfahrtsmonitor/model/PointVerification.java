package de.eos.uptrade.abfahrtsmonitor.model;

import java.util.ArrayList;
import java.util.List;

public class PointVerification {

		private String responseId;
		private Point point;
		private DateTime desiredDatetime;
		private List<Point> verifiedPoints;
		
		public PointVerification(Point point, DateTime desiredDatetime) {
			this.responseId = null;
			this.point = point;
			this.desiredDatetime = desiredDatetime;
			this.verifiedPoints = new ArrayList<Point>();
		}

		public String getResponseId() {
			return responseId;
		}

		public void setResponseId(String responseId) {
			this.responseId = responseId;
		}

		public Point getPoint() {
			return point;
		}

		public void setPoint(Point point) {
			this.point = point;
		}

		public DateTime getDesiredDatetime() {
			return desiredDatetime;
		}

		public void setDesiredDatetime(DateTime desiredDatetime) {
			this.desiredDatetime = desiredDatetime;
		}

		public List<Point> getVerifiedPoints() {
			return verifiedPoints;
		}

		public void setVerifiedPoints(List<Point> verifiedPoints) {
			this.verifiedPoints = verifiedPoints;
		}
}
