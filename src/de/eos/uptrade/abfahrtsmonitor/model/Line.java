package de.eos.uptrade.abfahrtsmonitor.model;


public class Line {
	
	private String lineId;
	private String vehicleType;
	private int vehicleCode;
	private String name;
	private Point terminus;
	private String direction;
	public String getLineId() {
		return lineId;
	}
	public void setLineId(String lineId) {
		this.lineId = lineId;
	}
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	public int getVehicleCode() {
		return vehicleCode;
	}
	public void setVehicleCode(int vehicleCode) {
		this.vehicleCode = vehicleCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Point getTerminus() {
		return terminus;
	}
	public void setTerminus(Point terminus) {
		this.terminus = terminus;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	
	
	
}
