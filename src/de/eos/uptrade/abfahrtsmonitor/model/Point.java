package de.eos.uptrade.abfahrtsmonitor.model;

public class Point {

	public static final int STATION_POINT = 0;
	
	private String pointId;
	private int type;
	private String name;
	private String region;
	
	public Point() { }
	
	public Point(String _pointId, String _name, String _region) {
		pointId = _pointId;
		name = _name;
		region = _region;
		type = STATION_POINT;
	}

	public String getPointId() {
		return pointId;
	}

	public void setPointId(String pointId) {
		this.pointId = pointId;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}
	
	
	
}
