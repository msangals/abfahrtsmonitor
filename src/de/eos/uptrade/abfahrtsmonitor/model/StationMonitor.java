package de.eos.uptrade.abfahrtsmonitor.model;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class StationMonitor {
	
	@SerializedName("responseId")
	private int mResponseId;
	
	@SerializedName("point")
	private Point mPoint;
	
	@SerializedName("desiredDatetime")
	private DateTime mDatetime;
	
	@SerializedName("stationEvents")
	private List<StationEvent> mStationEvents;
	
	public StationMonitor(Point point, DateTime datetime) {
		mPoint = point;
		mDatetime = datetime;
		mStationEvents = new ArrayList<StationEvent>();
	}

	public int getResponseId() {
		return mResponseId;
	}

	public void setResponseId(int responseId) {
		this.mResponseId = responseId;
	}
	
	public Point getPoint() {
		return mPoint;
	}

	public void setPoint(Point point) {
		this.mPoint = point;
	}

	public DateTime getDatetime() {
		return mDatetime;
	}

	public void setDatetime(DateTime datetime) {
		this.mDatetime = datetime;
	}

	public List<StationEvent> getStationEvents() {
		return mStationEvents;
	}

	public void setStationEvents(List<StationEvent> stationEvents) {
		this.mStationEvents = stationEvents;
	}
	
	
	
	
}
