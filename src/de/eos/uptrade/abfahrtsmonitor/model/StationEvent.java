package de.eos.uptrade.abfahrtsmonitor.model;

public class StationEvent {
	
	private VehicleEvent vehicleEvent;
	private VehicleRoute route;
	
	public VehicleEvent getVehicleEvent() {
		return vehicleEvent;
	}
	public void setVehicleEvent(VehicleEvent vehicleEvent) {
		this.vehicleEvent = vehicleEvent;
	}
	public VehicleRoute getRoute() {
		return route;
	}
	public void setRoute(VehicleRoute route) {
		this.route = route;
	}	
	
	
	
}
