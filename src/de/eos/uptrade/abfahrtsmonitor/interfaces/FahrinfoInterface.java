package de.eos.uptrade.abfahrtsmonitor.interfaces;

public interface FahrinfoInterface {

	public static final String FIFFY_SALT = "vc9T8F3pwtflGrO1YVnhPcTeOwqt1pNUSBKy5hig8Li67dmbLw0DC7O5RGgbGCp";
	public static final String FIFFY_APPLICATION_KEY = "abfahrtstafel_intern_V7xdGJ3zvNr31G0Eoee9DtHl8hKmxTJbqPxtqV1cTpBFFGvpfjQQXgYq2qDXLzM";
	
	public static final String FIFFY_HOST = "fiffi1.fahrinfoapp.de";
	public static final String FIFFY_STATION_MONITOR_URL_PATH = "/services.php/services/stationMonitor.json";
	public static final String FIFFY_POINT_VERIFICATION_URL_PATH = "/services.php/services/pointVerification.json";
	
//	public static final String FIFFY_HOST = "pornfony.com";
//	public static final String FIFFY_STATION_MONITOR_URL_PATH = "/abfahrtsmonitor/index.php";
	public static final int FIFFY_REQUEST_TIME = 30; // Seconds
	
	public static final int FIFFY_DEPARTURE_TIME_LIMIT = 1200; //seconds
	public static final int FIFFY_DEPARTURE_TIME_LIMIT_LAST = 60; // Seconds
	public static final String FIFFY_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
	
}