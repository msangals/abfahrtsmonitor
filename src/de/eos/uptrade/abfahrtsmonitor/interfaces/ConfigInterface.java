package de.eos.uptrade.abfahrtsmonitor.interfaces;

public class ConfigInterface {
	
	public static final String INTENT_POINT_ID = "INTENT_POINT_ID";
	public static final String INTENT_POINT_NAME = "INTENT_POINT_NAME";
	
	public static final String SHARED_PREFERENCES_POINT_ID = "SHARED_PREFERENCES_POINT_ID";
	public static final String SHARED_PREFERENCES_POINT_NAME = "SHARED_PREFERENCES_POINT_NAME";
	
	
	
}
