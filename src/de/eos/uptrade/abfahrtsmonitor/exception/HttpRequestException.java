package de.eos.uptrade.abfahrtsmonitor.exception;

public class HttpRequestException extends Exception {

	private static final long serialVersionUID = 261853846456886199L;

	public HttpRequestException(String message) {
		super(message);
	}

	
	public HttpRequestException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
