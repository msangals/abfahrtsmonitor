package de.eos.uptrade.abfahrtsmonitor.utils;

import java.io.UnsupportedEncodingException;

public class StringUtils {

	public static String getHexString(byte[] raw) throws UnsupportedEncodingException {
		char[] hex = new char[2 * raw.length];
		int index = 0;

		for (byte b : raw) {
			int v = b & 0xFF;
			hex[index++] = Character.forDigit(v >>> 4, 16);
			hex[index++] = Character.forDigit(v & 0xF, 16);
		}
		
		return new String(hex);
	}
	
	public static String replaceUmlaut(String input) {
		 
	     //replace all lower Umlauts
	     String o_strResult =
	             input
//	             .replaceAll("�", "ue")
//	             .replaceAll("�", "oe")
//	             .replaceAll("�", "ae")
	             .replaceAll("�", "ss");
	 
	     //first replace all capital umlaute in a non-capitalized context (e.g. �bung)
//	     o_strResult =
//	             o_strResult
//	             .replaceAll("�(?=[a-z���� ])", "Ue")
//	             .replaceAll("�(?=[a-z���� ])", "Oe")
//	             .replaceAll("�(?=[a-z���� ])", "Ae");
//	 
//	     //now replace all the other capital umlaute
//	     o_strResult =
//	             o_strResult
//	             .replaceAll("�", "UE")
//	             .replaceAll("�", "OE")
//	             .replaceAll("�", "AE");
	 
	     return o_strResult;
	 }
	
}
