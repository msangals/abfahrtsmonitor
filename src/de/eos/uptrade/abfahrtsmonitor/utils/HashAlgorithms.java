package de.eos.uptrade.abfahrtsmonitor.utils;

import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import de.eos.uptrade.abfahrtsmonitor.debug.LogCat;

/**
 * The HashAlgorithm class provides static method for calculating different Hashes (like SHA1, Hmac-SHA1)
 * 
 * @author btreger
 *
 */
public class HashAlgorithms {
	private static final String TAG = HashAlgorithms.class.getSimpleName();
	
	/**
	 * Returns Hash-based message authentication code (HMAC) for the given value and secret key.
	 * As underlying hash function the Secure Hash Algorithm (SHA1) is used.
	 * 
	 * @link http://en.wikipedia.org/wiki/Hash-based_message_authentication_code
	 * @link http://en.wikipedia.org/wiki/Secure_Hash_Algorithm
	 * 
	 * @param value String which should be hashed 
	 * @param key Secret key which is used to initialize the HMAC object
	 * @return Hmac-SHA1-Hash as hexadecimal String
	 */
	public static String getHmacSha1(String value, String key){
		String hexString = "";
		try {
            // convert given key to byte array and instance SecretKeySpec object
            final  byte[] keyBytes = key.getBytes();           
            final SecretKeySpec secrectKey = new SecretKeySpec(keyBytes, "HmacSHA1");
            // Get an hmac_sha1 Mac instance and initialize it with the secrect key
            final Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(secrectKey);
            // Compute the hash of given values as byte array
            final byte[] rawHmac = mac.doFinal(value.getBytes());
            // Convert hmac byte array to hex String
            hexString = ConvertUtils.convertByteArrayToHexString(rawHmac);
        } catch (IllegalArgumentException e1) {
			LogCat.e(TAG, "IllegalArgumentException in getHmacSha1(): " + e1.getMessage());
        } catch (NoSuchAlgorithmException e2) {
        	LogCat.e(TAG, "NoSuchAlgorithmException in getHmacSha1(): " + e2.getMessage());
        } catch (InvalidKeyException e3){
        	LogCat.e(TAG, "InvalidKeyException in getHmacSha1(): " + e3.getMessage());
		} catch (IllegalStateException e4) {
			LogCat.e(TAG, "IllegalStateException in getHmacSha1(): " + e4.getMessage());
		}
		return hexString;
	}
	
	/**
	 * Returns Secure Hash Algorithm (SHA1) for the given value.
	 * 
	 * @link http://en.wikipedia.org/wiki/Secure_Hash_Algorithm
	 * 
	 * @param value String which should be hashed
	 * @return SHA1-Hash as hexadecimal String
	 */
	public static String getSha1(String value) {
		String hexString = "";
		try {
			final MessageDigest md = MessageDigest.getInstance("SHA-1");
			md.update(value.getBytes());
			// compute hash 
		    final byte rawSha1[] = md.digest();
		    // convert byte array to hex string
		    hexString = ConvertUtils.convertByteArrayToHexString(rawSha1);	    
		} catch (NoSuchAlgorithmException e) {
			LogCat.e(TAG, "NoSuchAlgorithmException in getSha1(): " + e.getMessage() );
		}
	    return hexString;
	}
}