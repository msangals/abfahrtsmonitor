package de.eos.uptrade.abfahrtsmonitor.utils;

import java.lang.reflect.Method;

import android.content.Context;
import android.provider.Settings.Secure;
import de.eos.uptrade.abfahrtsmonitor.debug.LogCat;

public class DeviceInfo {
	private static final String TAG = DeviceInfo.class.getSimpleName();
	private static String mUniqueId;
	
	public static String getUniqueId(Context context){
		if (mUniqueId == null) {
			final String androidId = getAndroidId(context);
			// Android 2.2 (API 8) bug: devices always uses 9774d56d682e549c as Android ID
			// @see https://code.google.com/p/android/issues/detail?id=10603
			if (!androidId.equals("9774d56d682e549c")){
				mUniqueId = androidId;
			} else {
				// Fallback 1
				final String serial = getDeviceSerial();
				if (serial != null && serial.length() > 0) {
					mUniqueId = serial;
				}
				// Fallback 2
				else {
					mUniqueId = HashAlgorithms.getSha1(getDeviceName());
				}
			}
		}
		return mUniqueId;
	}
	
	public static String getAndroidId(Context context){
		String androidId = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
		LogCat.v(TAG, "AndroidId: " + androidId);
		return androidId;
	}
	
	public static String getDeviceSerial(){
		String serial = null; 
		try {
		    Class<?> c = Class.forName("android.os.SystemProperties");
		    Method get = c.getMethod("get", String.class);
		    serial = (String) get.invoke(c, "ro.serialno");
		} catch (Exception e) {}
		LogCat.v(TAG, "Serial: " + serial);
		return serial;
	}
	
	public static String getDeviceName(){
		String device_name = android.os.Build.MANUFACTURER + " " + android.os.Build.DEVICE + " - '" + android.os.Build.MODEL + "'";
		LogCat.v(TAG, "DeviceName: " + device_name);
		return device_name;
	}
		
	public static String getDeviceOS(){
		String device_os = android.os.Build.DISPLAY;
		LogCat.v(TAG, "DeviceOS: " + device_os);
		return device_os;
	}
	
	public static String getDeviceOSVersion(){
		String device_version = android.os.Build.VERSION.RELEASE + ", SDK: " + android.os.Build.VERSION.SDK_INT;
		LogCat.v(TAG, "DeviceOSVersion: " + device_version);
		return device_version;
	}
	
	public static String getAppPath(Context context){
		return context.getApplicationContext().getFilesDir().getParentFile().getPath();
	}
}