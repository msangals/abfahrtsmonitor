package de.eos.uptrade.abfahrtsmonitor.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Connectivity {

	public static ConnectivityManager manager;

	/**
	 * determines if the connection is available and is connected or connecting
	 * 
	 * @param context
	 *            the Context needed to get NetworkInfos
	 * @return true if a connection is available and is connected or connecting
	 * @see {@link NetworkInfo#isAvailable()}.
	 *      {@link NetworkInfo#isConnectedOrConnecting()}
	 */
	public static boolean isOnline(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo[] infos = cm.getAllNetworkInfo();
		for (NetworkInfo info : infos) {
			if (info != null && info.isAvailable()
					&& info.isConnectedOrConnecting()) {
				return true;
			}
		}
		NetworkInfo ani = cm.getActiveNetworkInfo();
		if (ani != null && ani.isAvailable() && ani.isConnectedOrConnecting()) {
			return true;
		}

		return false;

	}
}
